<?php
require_once("inc/core.php");
$core = new core;


?>

<html>
<head>
   <title>ODB2Graph</title>
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
   <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600' rel='stylesheet' type='text/css'>
   <link rel="stylesheet" href="<?php echo base_url("css/style.css") ?>">
</head>
<body>

      <?php echo core::load_view("inc/views/" . $core->mod . ".php", $core->mod_data) ?>


      <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="http://code.highcharts.com/highcharts.js"></script>
      <script src="http://code.highcharts.com/modules/data.js"></script>
      <script src="http://code.highcharts.com/modules/exporting.js"></script>
      <script type="text/javascript" src="<?php echo base_url("js/onload.js") ?>"></script>


</body>
</html>

$(document).ready(function(){


    var chart = $('#container').highcharts({
        data: {
            table: 'datatable'
        },
        chart: {
            type: 'line',
            zoomType: 'x',
            events: {
               load: function(event) {

                  var series = this.series; //scope

                  //hide high values by default (RPM)
                   for (var i = 0; i < this.series.length; i++) {
                      if(this.series[i].dataMax > 2000)
                        this.series[i].hide();
                   }

               }
           }
        },
        title: {
            text: ''
        },
        yAxis: {
            allowDecimals: true,
            title: {
                text: 'Units'
            }
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + '</b><br/>' +
                    this.point.y + ' ' + this.point.name.toLowerCase();
            }
        }
    });

})

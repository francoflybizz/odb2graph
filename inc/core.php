<?php

	class Core{

		public $mod;
		public $mod_data; //info da página actual
		public $meta;


		function __construct(){

			error_reporting(0);

			session_start();

			self::load_general_functions();

			$this->set_mod();

			$this->load_module();

			//definir content type + encoding - plain html here
			header("Content-type: text/html; charset=utf-8");

		}

		function load_class($class){
			require_once("inc/modules/".strtolower($class).".mod.php");
		}

		function set_mod(){
			$this->mod = (isset($_GET["mod"])) ? $_GET["mod"] : "home";
		}

		function load_module(){

			switch ($this->mod) {

				default:
					$file_path = "inc/modules/".$this->mod.".mod.php";
					if(file_exists($file_path)){
						require_once($file_path);
						$this->mod_data = new $this->mod;
					}

					break;
			}

		}

		function load_view($file, $data = false){

			if(file_exists($file)){
				if($data){

					//se já for um object deve ser retornado sem alteração
					if(is_object($data)){

					}

					else{
						//criar um object temporario
						$temp_object = new stdClass();
						foreach ($data as $key => $value) {
							$temp_object->$key = $value;
						}

						//passar o core na própria data - deve haver melhor forma de fazer isto. não é grave porque php passa por referência
						$temp_object->core = $this;

						//limpar $data colocando os valores do object temporario
						unset($data);
						$data = $temp_object;
					}

				}

				ob_start();
				include($file);

				$output = ob_get_clean();
				echo $output;
			}

			else echo "File <b>".$file."</b> not found";

		}

		//define functions de utilização geral (root, friendly-url, etc)
		function load_general_functions(){

			function var_bump($var){
				echo '<div style="z-index: 999999; position: fixed; right:10px; top:10px; padding:15px; background: rgba(0,0,0,0.8); color:#fff; width: 300px; font-size: 11px; font-family: Consolas; line-height: 15px; border-radius: 3px; height: 300px; overflow-y: scroll;"><pre>'; var_dump($var); echo '</pre></div>';
			}

			function base_url($url = false){
				if($_SERVER["HTTP_HOST"] == "localhost")
					$host = "http://localhost/odbgraph";
				else
					$host = "http://".$_SERVER["HTTP_HOST"]."/odbgraph";

				return $host."/".$url;
			}

			function base_path($file){
				$path = "C:/xampp/htdocs/odbgraph/";
				return $path . $file;
			}

		}

	}

?>

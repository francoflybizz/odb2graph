<?php

/**
 * Graph
 */
class Graph
{

   function __construct()
   {
      # code...
      if(!empty($_FILES["file"]) && !empty($_FILES["file"]["tmp_name"])){
         $file_path = $_FILES["file"]["tmp_name"];

         $data = $this->load_csv($file_path);
         $this->table_data = $this->setup_table($data);

      }
   }

   //setup table data according to preset options (time interval, min, max values, etc)
   function setup_table($data){

      foreach ($data as $line => $collumn) {

         if($line == 0){
            $output["headers"] = $collumn;
         }
         else{
            $last_timestamp = isset($data[$line-1][0]) ? strtotime($data[$line-1][0]) : strtotime($data[$line][0]);
            $current_timestamp = strtotime($data[$line][0]);

            if($last_timestamp != $current_timestamp)
               $output["values"][] = $collumn;

         }

      }
      return $output;
   }

   //reads and returns data as an array of lines and collumns
   function load_csv($file_path){

      if(file_exists($file_path)){
         $handle = fopen($file_path, "r");
         while (($data = fgetcsv($handle)) !== FALSE) {
            $lines[] = $data;
         }

         return $lines;
      }
      else
         return false;
   }

}


?>

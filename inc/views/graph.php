<div class="container">


      <div class="panel panel-default">
         <div class="panel-heading"><strong>Output</strong></div>
         <div class="panel-body">

            <h4>Graph from file input</h4>

            <div class="alert alert-info" role="alert">Some lines may be disabled by default to provide a better reading. You can enable/disable data lines by clicking the labels beneath the graph.</div>

            <div id="container">
            <table id="datatable">
               <thead>
                  <tr>
                     <?php foreach ($data->table_data["headers"] as $collumn): ?>
                        <?php if ($collumn == "Device Time"): ?>
                           <th></th>
                        <?php else: ?>
                           <th><?php echo ($collumn) ?></th>
                        <?php endif; ?>

                     <?php endforeach; ?>
                  </tr>
               </thead>
               <tbody>
                  <?php foreach ($data->table_data["values"] as $line): ?>
                     <tr>
                        <?php foreach ($line as $collumn_index => $collumn): ?>
                           <?php if ($collumn_index == 0): ?>
                              <th><?php echo date("H:i:s", strtotime($collumn)) ?></th>
                           <?php else: ?>
                              <?php if ($collumn == "-"): ?>
                                 <td>0</td>
                              <?php else: ?>
                                 <td><?php echo $collumn ?></td>
                              <?php endif; ?>

                           <?php endif; ?>

                        <?php endforeach; ?>
                     <?php endforeach; ?>
                  </tr>
               </tbody>
            </table>
         </div>

         </div>
      </div>


</div>

</div>

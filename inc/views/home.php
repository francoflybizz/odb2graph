<div class="container">

   <div class="panel panel-default">
      <div class="panel-heading"><strong>Upload</strong></div>
      <div class="panel-body">

         <h4>Select the log file</h4>
         <form action="<?php echo base_url("graph") ?>" method="post" enctype="multipart/form-data" id="js-upload-form">
            <div class="form-inline">
               <div class="form-group">
                  <input type="file" name="file" id="js-upload-files" multiple="">
               </div>
               <button type="submit" class="btn btn-sm btn-primary" id="js-upload-submit">Upload file</button>
            </div>
         </form>

      </div>
   </div>
</div>
